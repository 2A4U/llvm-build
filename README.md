# llvm-build
A llvm.git plugin script for xbuild.

## Prerequisites
1. [xbuild scripts](https://gitlab.freedesktop.org/2A4U/xbuild) - `git clone https://gitlab.freedesktop.org/2A4U/xbuild`
2. [llvm](https://github.com/llvm-mirror/llvm) - `mkdir $HOME/xsrc` `cd xsrc` `git clone https://github.com/llvm-mirror/llvm.git` Note: xsrc is the default directory for user variable $sourceDirectory.

## Getting Started
1. Within $HOME directory type: `git clone https://gitlab.freedesktop.org/2A4U/llvm-build`
2. `linkplugin` - checks user variable $sourceDirectory for a match to plugin script. A symlink to matching script is created in $HOME/module_plugin.
3. `cdx` - an xbuild command.
4. `build llvm`